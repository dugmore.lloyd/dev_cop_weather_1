# Description  
Small Django CRUD application that interfaces with an OpenSource Weather API.


---
## Pre-requisites
- Have docker and docker-compose installed on machine

---

## Local Dev Environment Setup
- Copy **.env_example** to **.env**
- Build docker image:
  - ```docker-compose up --build```
- Application should now be running locally on: http://127.0.0.1

## Note: You only need to build the image once, you can run ```docker-compose up``` for consecutive local app deployments

---

## TESTING
### Django tests
python manage.py test

or 


#### Django tests directly in container
Exec into the weather_frontend container

```
docker exec -it weather_frontend bash
```

Run the below command

```
python manage.py test
```

To create coverage reports, run the below

```
coverage run --omit */site-packages/* manage.py test -v2
```

To view results of the report, run the below commands

```
coverage report

or

coverage html
```

