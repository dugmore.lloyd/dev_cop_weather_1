FROM python:3.6.8-alpine

ENV PYTHONUNBUFFERED 1

RUN apk add --update --no-cache \
    bash \
    g++ && \
    rm -rf /var/cache/apk/* && \
    apk add --update --no-cache \
    alpine-sdk \
    postgresql-libs && \
    apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev \
    && rm -rf /var/cache/apk/* && \
    curl -o /usr/local/bin/waitforit -sSL https://github.com/maxcnunes/waitforit/releases/download/v2.4.0/waitforit-linux_amd64 && \
    chmod +x /usr/local/bin/waitforit

RUN mkdir /code
WORKDIR /code

COPY requirements/requirements_base.txt /code/
RUN pip install -r requirements_base.txt

COPY . /code/

EXPOSE 8000

RUN chmod +x /code/docker-entrypoint.sh
ENTRYPOINT [ "/code/docker-entrypoint.sh" ]