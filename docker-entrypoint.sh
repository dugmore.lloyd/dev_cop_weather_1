#!/bin/bash -e

if [ "$1" == "run-tests" ]; then
    waitforit -host=${POSTGRES_HOST} -port=${POSTGRES_PORT} -timeout 30
    exec python manage.py test --no-input --parallel
elif [ $# -ne 0 ]; then
    echo '=================================='
    echo 'Waiting for database connection...'
    echo '=================================='
    waitforit -host=${POSTGRES_HOST} -port=${POSTGRES_PORT} -timeout 10
    exec "$@"
fi