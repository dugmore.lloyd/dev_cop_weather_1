from django.template import loader
import requests
from django.shortcuts import render, redirect
from .models import City
from django.conf import settings
from .forms import CityForm

def index(request):
    context = {
        'random_message': "Hello all! (phase9)",
    }
    return render(request, 'phase9/index.html', context)


def weather(request):

    err_msg = ''
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&APPID={}'

    # Concerned with the POST
    if request.method == 'POST':
        
        form = CityForm(request.POST)
        print(form)

        ########## 0. 
        # form.save()

        ########## 1. Make sure the input is not duplicate
        # if form.is_valid():
        #     new_city = form.cleaned_data['name']

        #     #Search the DB beforehand to ensure we dont find a city with the same name
        #     existing_count = City.objects.filter(name=new_city).count()
        #     print(f'Existing Count {existing_count}')

        #     if existing_count == 0:
        #         form.save()
        #     else:
        #         err_msg = 'That city already exists!'






        ########## 2. Make sure the input is not duplicate and is a valid city
        if form.is_valid():
            new_city = form.cleaned_data['name']

            #Search the DB beforehand to ensure we dont find a city with the same name
            existing_count = City.objects.filter(name=new_city).count()

            if existing_count == 0:
                response = requests.get(url.format(new_city, settings.WEATHER_API_KEY)).json()
                if response['cod'] == 200:
                    form.save()
                else:
                    err_msg = 'That is not a real city!'
            else:
                err_msg = 'That city already exists!'





    # Clear the form after resubmit
    form = CityForm()

    # Concerned with the GET
    cities = City.objects.all()
    weather_list = []

    for city in cities:
        response = requests.get(url.format(city.name, settings.WEATHER_API_KEY)).json()

        city_obj = {
            'city': city.name,
            'temperature': response['main']['temp'],
            'description': response['weather'][0]['description'],
            'icon': response['weather'][0]['icon'],
            'wind': response['wind'],
        }
        weather_list.append(city_obj)

    #### !IMPORTANT, remember to return the form in the CONTEXT
    context = {'weather_data': weather_list, 
                'form': form, 
                'err_msg': err_msg}
    return render(request, 'phase9/weather.html', context)


def about(request):
    context = {}
    return render(request, 'phase9/about.html', context)


def delete_city(request, city_name):
    print(f'IN DELETE >>> {city_name}')
    City.objects.get(name=city_name).delete()
    return redirect('weather')
