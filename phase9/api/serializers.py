from rest_framework import serializers

from phase9 import models


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.City
        fields = ('__all__')
