from django.urls import path

from phase9.api import views

app_name = 'phase9'


urlpatterns = [
    path('', views.CityListView.as_view(), name='list'),
]