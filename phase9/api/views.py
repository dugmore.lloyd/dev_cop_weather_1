from rest_framework import generics, permissions as dj_permissions
from rest_framework.views import APIView
from django.db import transaction

from phase9 import models
from phase9.api import serializers


class CityListView(generics.ListAPIView):
    serializer_class = serializers.CitySerializer
    permission_classes = (dj_permissions.IsAuthenticated,)

    def get_queryset(self):
        queryset = models.City.objects.all().order_by('name')
        return queryset

