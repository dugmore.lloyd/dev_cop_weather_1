from django.apps import AppConfig


class Phase9Config(AppConfig):
    name = 'phase9'
