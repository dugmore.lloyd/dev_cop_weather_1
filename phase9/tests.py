from django.test import TestCase

from .models import City

class CityTest(TestCase):
    def test_city_create(self):
        obj = City(name='Auckland')
        self.assertEqual(obj.name, 'Auckland')